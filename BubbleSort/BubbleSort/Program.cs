﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BubbleSort
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Укажите длину массива");
            int arrLength = int.Parse(Console.ReadLine());
            int[] arr = new int[arrLength];
            AddArrayValues(arr);
            ArraySort(arr);
            foreach (var item in arr)
                Console.Write($"{item} ");

            Console.ReadKey();
        }
        static void AddArrayValues(int[] arr)
        {
            Random randomNumber = new Random();
            for (int i = 0; i < arr.Length; i++)
                arr[i] = randomNumber.Next(arr.Length);
        }
        static void ArraySort(int[] arr)
        {
            int temp;
            for (int i = 0; i < arr.Length - 1; i++)
            {
                for (int j = 0; j < arr.Length - i - 1; j++)
                {
                    if (arr[j + 1] < arr[j])
                    {
                        temp = arr[j + 1];
                        arr[j + 1] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
        }
    }
}
